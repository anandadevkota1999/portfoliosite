<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AllController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('adminregister', [AllController::class, 'registerAdmin']);
Route::post('adminlogin', [AllController::class, 'adminLogin']);
Route::get('displayimage/{foldername}',[AllController::class, 'displayImage']);
Route::group( ['prefix' => 'admin','middleware' => ['auth:admin-api'] ],function(){
Route::post('addworkexperience', [AllController::class, 'createWorkExperience']);
Route::post('addevent', [AllController::class, 'createEvent']);
Route::post('addcarausels', [AllController::class, 'createCarausel']);
Route::get('admindetail/{id}', [AllController::class, 'viewAdmin']);
Route::get('carauseldetail/{id}', [AllController::class, 'viewCarausel']);
Route::get('getcarausels', [AllController::class, 'getCarausels']);
Route::put('carauselupdate/{id}', [AllController::class, 'editCarausel']);
Route::delete('carauseldelete/{id}', [AllController::class, 'deleteCarausel']);
Route::get('eventdetail/{id}', [AllController::class, 'viewEvent']);
Route::get('getallevents', [AllController::class, 'getAllEvents']);
Route::put('eventupdate/{id}', [AllController::class, 'editEvent']);
Route::delete('deleteevent/{id}', [AllController::class, 'deleteEvent']);
Route::post('addtimeline', [AllController::class, 'addTimeline']);
Route::get('gettimelines', [AllController::class, 'getTimelines']);
Route::get('timelinedetail/{id}', [AllController::class, 'timeLineDetail']);
Route::put('timelineupdate/{id}', [AllController::class, 'updateTimeline']);
Route::delete('timelinedelete/{id}', [AllController::class, 'deleteTimeline']);
Route::post('addgallery', [AllController::class, 'addGallery']);
Route::put('updategallery/{id}', [AllController::class, 'updateGallery']);
Route::delete('deletegallery/{id}', [AllController::class, 'deleteGallery']);
Route::get('gallerydetail/{id}', [AllController::class, 'galleryDetail']);
Route::get('allgalleries', [AllController::class, 'allGalleries']);
Route::post('addinterview', [AllController::class, 'addInterview']);
Route::put('updateinterview/{id}', [AllController::class, 'updateInterview']);
Route::delete('deleteinterview/{id}', [AllController::class, 'deleteInterview']);
Route::get('getinterview/{id}', [AllController::class, 'interviewDetail']);
Route::get('allinterviews', [AllController::class, 'AllInterviews']);
Route::post('addagenda', [AllController::class, 'addAgenda']);
Route::put('editagenda/{id}', [AllController::class, 'editAgenda']);
Route::delete('deleteagenda/{id}', [AllController::class, 'deleteAgenda']);
Route::get('agendadetail/{id}', [AllController::class, 'agendaDetail']);
Route::get('allagenda', [AllController::class, 'allAgendas']);
Route::post('changepassword/{id}', [AllController::class, 'changeAdminPassword']);


});
Route::get('getcarausels', [AllController::class, 'getCarausels']);
Route::get('gettimelines', [AllController::class, 'getTimelines']);
Route::get('allgalleries', [AllController::class, 'allGalleries']);
Route::get('getallevents', [AllController::class, 'getAllEvents']);
Route::get('allinterviews', [AllController::class, 'AllInterviews']);
Route::get('allagenda', [AllController::class, 'allAgendas']);




