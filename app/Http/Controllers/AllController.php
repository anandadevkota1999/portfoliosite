<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Models\Admin;
use App\Models\WorkExperience;
use App\Models\CarauselImage;
use App\Models\Event;
use App\Models\EventImage;
use App\Models\BioTimeline;
use App\Models\Gallery;
use App\Models\Interview;
use App\Models\Agenda;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Validator;
use Illuminate\Http\Request;
use Hash;
use Carbon\Carbon;



class AllController extends BaseController
{   
    public function registerAdmin(Request $req){
      try{
         $validator = Validator::make($req->all(), [
             'admin_name' => 'required',
             'email_address'=>'required|email|unique:admins',
             'password'=>'required|min:6',
             'confirm_password'=>'required|same:password'
        ]);
        if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
         $input = $req->all();
         $input['admin_name']=$req['admin_name'];
        $input['email_address']=$req['email_address'];         
         $input['password'] = Hash::make($req['password']);
         $input['created_at']=Carbon::now();
          $input['updated_at']=Carbon::now();
         $model=Admin::create($input);
          return $this->sendResponse($model,'Congrats!, Admin has been registered successfully!!');
         
      }
      catch ( \Exception $ex ){
      return $this->sendError(($ex->getMessage())); 
     } 
    }
    function viewAdmin($id){
        $get=Admin::where('id',$id)->get();
        return $this->sendResponse($get,'Admin Details shown successfully!!'); 
    }
    public function adminLogin(Request $request){
        $validator = Validator::make($request->all(), [
            'email_address' => 'required|email',
            'password'=>'required|min:6']);
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
        } 
     if (Auth::guard('admin')->attempt(['email_address'=> $request->email_address, 'password' => $request->password])) {
       config(['auth.guards.api.provider' => 'admin_info']);  
       $admin=Auth::guard('admin')->user();
       $success['id']=$admin->id;
       $success['token'] =  $admin->createToken('MyApp')->accessToken;
      return $this->sendResponse($success,'Login Token created successfully!!'); 
     }
     else{
         return $this->sendError('provided credentials are wrong!');
      }
    }
    function changeAdminPassword($id,Request $request){
       $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:6',
            'new_password'=>'required|min:6|different:old_password',
            'confirm_password'=>'required|same:new_password',
        ]);
        if($validator->fails()){
           $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string); 
        }
     if (Auth::guard('admin-api')->check()) {
        $showall=Admin::find($id);
        if(is_null($showall)){
          return $this->sendError('There is no admin to show!!');
        }
     else{
        $getpass=$showall->password;
      if(\Hash::check($request->old_password,$getpass) ){
       $showall->password=bcrypt($request->new_password);
       $query=Admin::where( 'id' , $id)->update( array( 'password' => $showall->password,'updated_at'=>Carbon::now()));
       return $this->sendResponse($query,'Admin password is successfully changed!');
     } 
     else{
         return $this->sendError('Old password does not match!!'); 
       }  
      }
     } 
    }
    function createWorkExperience(Request $req){
           $validator = Validator::make($req->all(), [
            'title' => 'required','description'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         $input=$req->all();
         if($req->hasFile('image')){
             $file='WorkExperience';
             $req->image->store($file, 'local');
             $input['image']=$req->image->hashName();
            }
            $input['created_at']=Carbon::now();
          $input['updated_at']=Carbon::now();
         $store=WorkExperience::create($input);
         return $this->sendResponse($store,'Work experience is created successfully!!');    
    }
    function createCarausel(Request $req){
            $validator = Validator::make($req->all(), [
            'title' => 'required','image'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         $input=$req->all();
         if($req->hasFile('image')){
             $file='CarauselImages';
             $req->image->store($file, 'local');
             $input['image']=$req->image->hashName();
            }
         $input['created_at']=Carbon::now();
         $input['updated_at']=Carbon::now();
         $store=CarauselImage::create($input);
         return $this->sendResponse($store,'Work experience is created successfully!!');   
    }
    function viewCarausel($id){
        $get=CarauselImage::where('id',$id)->first();
        return $this->sendResponse($get,'Carausel Details shown successfully!!'); 
    }
    function getCarausels(){
        $get=CarauselImage::get();
        return $this->sendResponse($get,'All Carausels shown successfully!!'); 
    }
    function editCarausel($id,Request $req){
         $validator = Validator::make($req->all(), [
            'title' => 'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         $get=CarauselImage::where('id',$id)->update(['title'=>$req['title'],'description'=>$req['description'],'source'=>$req['source'],'updated_at'=>Carbon::now()]);
         if($req->hasFile('image')){
             $file='CarauselImages';
             $req->image->store($file, 'local');
             $get=CarauselImage::where('id',$id)->update(['image'=>$req->image->hashName(),'updated_at'=>Carbon::now()]);
            }
         return $this->sendResponse('done','Carausel updated successfully!!');   
    }
    function deleteCarausel($id){
      $get=CarauselImage::where('id',$id)->first();
      if(is_null($get)){
        return $this->sendError('Record not found!');
      }
      $get->delete();
      return $this->sendResponse('done','Carausel deleted successfully!!');
    }
    function createEvent(Request $req){
         $validator = Validator::make($req->all(), [
            'title' => 'required','image'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
          $input=$req->all();
         $input['created_at']=Carbon::now();
         $input['updated_at']=Carbon::now();
         $store=Event::create($input);
         if($req->hasFile('image')){
          $images=$req->file('image');
          foreach($images as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $name='EventImages';
            $image->storeAs($name,$has, 'local');
            $get=Event::orderBy('id','desc')->first();
            $id=$get->id;
            $model=new EventImage();
            $model->event_id=$id;
            $model->image=$has;
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->save();
          }
         }
         return $this->sendResponse($store,'event created successfully!!');
    }
    function getAllEvents(){
        $get=Event::with('images')->get();
        return $this->sendResponse($get,'all Events shown successfully!!');
    }
    function viewEvent($id){
        $get=Event::where('id',$id)->with('images')->first();
        return $this->sendResponse($get,'Event Details shown successfully!!'); 
    }
    public function displayImage($foldername,Request $req){
     $subfolder=$req['subfolder'];
     $filename=$req['filename'];
     if($req['subfolder'] && $req['filename']){
     $path=storage_path('app/'.$foldername.'/'.$subfolder.'/'.$filename); 
     if (!File::exists($path)) {
     abort(404);
     }
     $file = File::get($path);
     $type = File::mimeType($path);
     $response = Response::make($file, 200);
     $response->header("Content-Type", $type);
     return $response; 
     }
     if($req['filename']){
     $path=storage_path('app/'.$foldername.'/'.$filename);
     if (!File::exists($path)) {
     abort(404);
     }
     $file = File::get($path);
     $type = File::mimeType($path);
     $response = Response::make($file, 200);
     $response->header("Content-Type", $type);
     return $response;   
     }
    }
    function editEvent($id,Request $req){
        $validator = Validator::make($req->all(), [
            'title' => 'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
      $change=Event::where('id',$id)->update([
       'title'=>$req['title'],'description'=>$req['description'],'date'=>$req['date'],'updated_at'=>Carbon::now()
      ]);
      if($req->hasFile('image')){
           $images=$req->file('image');
          foreach($images as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $name='EventImages';
            $image->storeAs($name,$has, 'local');
            $get=EventImage::where('event_id',$id)->update(['image'=>$has,'updated_at'=>Carbon::now()]);
          }
     
      }
        return $this->sendResponse('done','Event updated successfully!!');  
    }
    function deleteEvent($id){
        $eventimages=Event::where('id',$id)->with('images')->first();
        if(is_null($eventimages)){
            return $this->sendError('Record not found!!');
        }
        $image=EventImage::where('event_id',$id)->first();
        if($image){
         $image->delete();   
        }
        $eventimages->delete();
        return $this->sendResponse('done','Event deleted successfully!!'); 
    }
    function addTimeline(Request $req){
        $input=$req->all();
          $validator = Validator::make($input, [
            'date' => 'required','description'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         $input['created_at']=Carbon::now();
         $input['updated_at']=Carbon::now();
         $store=BioTimeline::create($input);
         return $this->sendResponse($store,'Timeline added successfully!!');
    }
    function getTimelines(){
        $get=BioTimeline::get();
        return $this->sendResponse($get,'All timelines shown successfully!!');
    }
    function timeLineDetail($id){
        $get=BioTimeline::where('id',$id)->first();
        return $this->sendResponse($get,'Timeline details shown successfully!!');
    }
    function updateTimeline($id,Request $req){
            $validator = Validator::make($req->all(), [
            'date' => 'required','description'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         $update=BioTimeline::where('id',$id)->update(['date'=>$req['date'],'description'=>$req['description'],'updated_at'=>Carbon::now()]);
         return $this->sendResponse('done','Timeline updated successfully!!');
    }
    function deleteTimeline($id){
       $get=BioTimeline::where('id',$id)->first();
       if(is_null($get)){
        return $this->sendError('Record not found!');
       }
       $get->delete();
       return $this->sendResponse('done','Timeline deleted successfully!!');
    }
    function addGallery(Request $req){
         $input=$req->all();
          $validator = Validator::make($input, [
            'image' => 'required','description'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         if($req->hasFile('image')){
             $file='GalleryImages';
             $req->image->store($file, 'local');
             $input['image']=$req->image->hashName();
            }
         $input['created_at']=Carbon::now();
         $input['updated_at']=Carbon::now();
         $store=Gallery::create($input);
         return $this->sendResponse($store,'Image saved in Gallery successfully!!');
    }
    function updateGallery($id,Request $req){
      $get=Gallery::where('id',$id)->first();  
      if($req->hasFile('image')){
             $file='GalleryImages';
             $req->image->store($file, 'local');
            $get->update(['image'=>$req->image->hashName(),'updated_at'=>Carbon::now()]);
        }  
      $get->update(['tag'=>$req['tag'],'description'=>$req['description'],'updated_at'=>Carbon::now()]);
      return $this->sendResponse('done','Record updated successfully!!');
    }
    function deleteGallery($id){
        $get=Gallery::where('id',$id)->first();
        if(is_null($get)){
            return $this->sendError('Record not found!');
        }
        $get->delete();
        return $this->sendResponse('done','Record deleted successfully!!');
    }
    function galleryDetail($id){
        $get=Gallery::where('id',$id)->first();
        return $this->sendResponse($get,'Gallery details show successfully!!');
    }
    function allGalleries(){
      $get=Gallery::get();
      return $this->sendResponse($get,'All Gallery content shown successfully!!');
    }
    function addInterview(Request $req){
         $input=$req->all();
          $validator = Validator::make($input, [
            'image' => 'required','title'=>'required','source_url'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         if($req->hasFile('image')){
             $file='InterviewImages';
             $req->image->store($file, 'local');
             $input['image']=$req->image->hashName();
            }
         $input['created_at']=Carbon::now();
         $input['updated_at']=Carbon::now();
         $store=Interview::create($input);
         return $this->sendResponse($store,'Interview added successfully!!');
    }
    function updateInterview($id,Request $req){
         $input=$req->all();
         $validator = Validator::make($input, ['title'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         $get=Interview::where('id',$id)->first();
         if($req->hasFile('image')){
             $file='InterviewImages';
             $req->image->store($file, 'local');
             $get->update(['image'=>$req->image->hashName(),'updated_at'=>Carbon::now()]);             
            }
         $get->update(['title' =>$req['title'],'description'=>$req['description'],'source_url'=>$req['source_url'],'updated_at'=>Carbon::now()]);
         return $this->sendResponse('done','Interview updated successfully!!');
    }
    function deleteInterview($id){
       $get=Interview::where('id',$id)->first();
       if(is_null($get)){
        return $this->sendError('Record not found!');
       }
       $get->delete();
       return $this->sendResponse('done','Record deleted successfully!!');
    }
    function interviewDetail($id){
       $get=Interview::where('id',$id)->first();
       return $this->sendResponse($get,'Interview details shown successfully!!');
    }
    function AllInterviews(){
        $get=Interview::get();
        return $this->sendResponse($get,'All Interviews shown successfully!!');
    }
    function addAgenda(Request $req){
        $input=$req->all();
         $validator = Validator::make($input, [
            'title'=>'required','description'=>'required','image'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
         if($req->hasFile('image')){
             $file='AgendaImages';
             $req->image->store($file, 'local');
             $input['image']=$req->image->hashName();
            }
         $input['created_at']=Carbon::now();
         $input['updated_at']=Carbon::now();
         $store=Agenda::create($input);
         return $this->sendResponse($store,'Agenda added successfully!!');
    }
    function editAgenda($id,Request $req){
        $input=$req->all();
         $validator = Validator::make($input, [
            'title'=>'required','description'=>'required']);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
             $string=implode(",",$geterror);
             return $this->sendError($string);
         }
        $get=Agenda::where('id',$id)->first(); 
         if($req->hasFile('image')){
             $file='AgendaImages';
             $req->image->store($file, 'local');
             $get->update(['image'=>$req->image->hashName(),'updated_at'=>Carbon::now()]);
            }
        $get->update(['title'=>$req['title'],'description'=>$req['description'],'updated_at'=>Carbon::now()]);
        return $this->sendResponse('done','Agenda updated successfully!!');
    }
    function deleteAgenda($id){
       $get=Agenda::where('id',$id)->first();
       if(is_null($get)){
         return $this->sendError('Record not found!');
       }
       $get->delete();
       return $this->sendResponse('done','Record deleted successfully!!');
    }
    function agendaDetail($id){
        $get=Agenda::where('id',$id)->first();
        return $this->sendResponse($get,'Agenda details shown successfully!!');
    }
    function allAgendas(){
        $get=Agenda::get();
        return $this->sendResponse($get,'All Agendas shown successfully!!');
    }






    





}
