<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;

class Gallery extends Model implements AuthContract
{
    use HasFactory, Notifiable, HasApiTokens, Authenticatable;

   protected $fillable = ['image','description','tag','created_at','updated_at'];
}
