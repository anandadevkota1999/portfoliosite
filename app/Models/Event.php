<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
use App\Models\EventImage;


class Event extends Model implements AuthContract
{
    use HasFactory, Notifiable, HasApiTokens, Authenticatable;

   protected $fillable = ['title','description','date','created_at','updated_at'];

   public function images()
    {
        return $this->hasMany(EventImage::class,'event_id');
    } 
}



















