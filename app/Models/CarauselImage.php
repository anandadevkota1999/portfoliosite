<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;

class CarauselImage extends Model implements AuthContract
{
    use HasFactory, Notifiable, HasApiTokens, Authenticatable;

   protected $fillable = ['title','description','source','image','created_at','updated_at'];
}



















