<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
use App\Models\Event;

class EventImage extends Model implements AuthContract
{
    use HasFactory, Notifiable, HasApiTokens, Authenticatable;

   protected $fillable = ['event_id','image','created_at','updated_at'];


   public function event()
    {
        return $this->belongsTo(Event::class,'id','event_id');
    }


}



















